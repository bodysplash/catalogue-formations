---
layout: formation
titre: Formation au développement piloté par les tests
resume: Apprendre à pratique le Test Driven Development au quotidien
formateurs:
 - nom: Jean-Baptiste Dusseaut
   email: jbdusseaut@arpinum.fr
 - nom: Charles Couillard
   email: charles.couillard@arpinum.fr
participants: 8
duree: 3 jours
code: arpi_2
prix: 2000
category: formations
---
Une formation qu'elle est bien pour la faire