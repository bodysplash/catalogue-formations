---
layout: formation
titre: Formation au développement web java par Arpinum
resume: Comment faire du web de bout en bout sans framework fullstack
formateurs :
 - nom: Jean-Baptiste Dusseaut
   email: jbdusseaut@arpinum.fr
 - nom: Charles Couillard
   email: charles.couillard@arpinum.fr
participants: 8
duree: 4 jours
code: arpi_1
prix: 2700
category: formations
---
Ceci est une formation dur à suivre, attention, vous êtes prévenus. Plutôt que de se dire, «chouette je fais du web, je vais utiliser <intérer ici votre framework à la mode>», posons nous la 
question de comment faire une application web qui se moque d'être du web.

